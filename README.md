# Steven Shaviro and Text #
* * *
## Emojis ##

* "Sometimes I must read and type extremley fast to keep up with rapid-fire chat room conversations" (Shaviro 7).
* Emojis can be used instead of words
* Instead of typing "that sounds good" we could shorten it with a thumbs up emoji
![](https://emojipedia-us.s3.amazonaws.com/thumbs/120/apple/96/thumbs-up-sign_1f44d.png)
* In the eyes of Shaviro perhaps we may be using emojis to save time just so we are able to spend more time connected on the network

![](https://emojipedia-us.s3.amazonaws.com/thumbs/72/apple/96/thinking-face_1f914.png)

* Emojis serve as a shorthand version of real text to help describe feelings, emotions, or even a replacement for real words/ phrases
* [Why Do We Use Emojis?](https://www.psychologytoday.com/blog/contemporary-psychoanalysis-in-action/201605/why-do-we-use-emojis)
* * *
## The Network ##

* "We are inclined to see nearly everything in the terms of connections and networks" (Shaviro 3).
* Example: When SLU's internet goes out
* Complete change of agenda; functionality depends on the network
![](https://pics.me.me/how-i-feel-when-theres-no-wifi-aming-10243217.png)

* Text can be coloured or written differently to highlight an important aspect of a sentence or phrase
* Notice how in the picture above **"No Wifi"** is in yellow text, making it stand out
* One can fully understand the gist of the meme without reading the full sentence, but only reading **"No WiFi"** and looking at the picture
* Again, this relates back to Shaviro refferencing about people trying to type fast in order to save time so we can stay connected to the network
* In this scenario, a certain text is *highlighted* so we don't have to spend time reading the whole sentence
* When text is itallicized, boldded, or coloured, it draws the human eye
* * *
## Auto Correct ##
* "The network offers us continual feedback" (Shaviro 31).
* Auto correct has the ability to save us time... again, so we have more time to stay *connected* to the netowrk
* Technology actually gives us feedback for what it thinks is better; the system's way is superior to the human's way
* Auto correct changes what humans type in replacement for what the network wants
* Most humans actually trust the network even though it is also imperfect
* [Auto Correct Fails](https://www.buzzfeed.com/erinchack/the-most-concerning-autocorrect-fails-of-all-time?utm_term=.uvkJnqlZW#.csAJ7omRl)
* * *
### The Network is in Control ###
![](http://www.techdefencelabs.com/public/img/services/network-penetration.jpg)





